var http = require('http');
var fs = require('fs');
var url = require('url');
var qs = require('querystring');
var template = require('./lib/template.js');
var path = require('path');
var sanitizeHtml = require('sanitize-html');
var express = require('express');
var app = express();
var requestIp = require('request-ip');
var moment = require('moment');

var replaceall = require("replaceall");

var formidable = require('formidable');
const multer = require('multer');
const XLSX = require('xlsx');
var ori_path = __dirname;

const storage = multer.diskStorage({
	destination(req, file, callback) {
		callback(null, 'uploads');
	},
	filename(req, file, callback) {
		console.log(file.originalname);
		callback(null,file.originalname);

	}
});

app.use(express.static('uploads'));

const upload = multer({
	storage,
	limits:{
		files: 10,
		fileSize: 1024 * 1024 * 1024, 
	}
});

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get('/readlotto', function(req, res){
		var workbook = XLSX.readFile(`uploads/excel.xls`);
		var sheet_name = workbook.SheetNames[0];
		worksheet = workbook.Sheets[sheet_name];

		console.log(`uploads/excel.xls`);
		console.log(sheet_name);
		// console.log(worksheet);
		// console.log(worksheet['A1']['w']);
		let count = worksheet['B4']['v'];
		console.log(count);
		var text = "";
		for (let i = 0; i < count; i++) {
			let j = 4+i;
			var datetime = replaceall('.', '-', `${worksheet[`C${j}`]['v']}`);

			var gubun = ['E','G','I','K','M'];
			var gubuns = ['D','F','H','J','L'];
			var price = [];
			var winner = [];
			for (var p = 0; p < 5; p++) {
				price[p] = replaceall('원', '', `${worksheet[`${gubun[p]}${j}`]['v']}`);
				price[p] = replaceall(',', '', price[p]);

				if(worksheet[`${gubuns[p]}${j}`]['v'] == '' || worksheet[`${gubuns[p]}${j}`]['v'] == undefined){
					worksheet[`${gubuns[p]}${j}`]['v'] = '0';
				}
				winner[p] = replaceall(',', '', `${worksheet[`${gubuns[p]}${j}`]['v']}`);

			}
			// console.log(`H열 : ${worksheet[`H${j}`]['v']}`);
			// console.log(`J number : ${j}`);
			// console.log(`${datetime}`);
			// console.log(`${price[1]}`);


			// text += worksheet[`B${j}`]['v']+','+worksheet[`N${j}`]['v']+','+worksheet[`O${j}`]['v']+','+worksheet[`P${j}`]['v']+','+worksheet[`Q${j}`]['v']+','+worksheet[`R${j}`]['v']+','+worksheet[`S${j}`]['v']+','+worksheet[`T${j}`]['v']+'\n';
			text += `${worksheet[`B${j}`]['v']},${worksheet[`N${j}`]['v']},${worksheet[`O${j}`]['v']},${worksheet[`P${j}`]['v']},${worksheet[`Q${j}`]['v']},${worksheet[`R${j}`]['v']},${worksheet[`S${j}`]['v']},${worksheet[`T${j}`]['v']},${datetime},${price[0]},${winner[0]},${price[1]},${winner[1]},${price[2]},${winner[2]},${price[3]},${winner[3]},${price[4]},${winner[4]}\n`;
		}
		// console.log(text);

		res.setHeader('Content-type', "application/octet-stream");

		res.setHeader('Content-disposition', 'attachment; filename=lotto.txt');

		res.send(text);

		// for(let item in worksheet){
		// 	console.log(item);
		// 	// console.log('당첨번호 : '+worksheet[`B`]['w']);
		// }

		// res.send(worksheet);
		// res.send(worksheet['A1']);
});

app.get('/getLotto', function(req, res){
	// var send_id = req.body.id;
	// var send_pass = req.body.pass;
	// res.send('params : '+req.params.id);

	var lottoUrl = 'https://dhlottery.co.kr/gameResult.do?method=allWinExel&gubun=byWin&nowPage=1&drwNoStart=1&drwNoEnd=9999';
	res.send(`${lottoUrl}`);
	res.redirect(`${lottoUrl}`);
	
});

app.get('/lottotxt',function(req,res){
	var html = '<form action="/lottoupload" method="post" enctype="multipart/form-data">' 
	          + 'file :'
	          + '<input type="file" name="xlxs_file">'
	          + '<input type="submit" value="Upload">'
	          + ' </form>';
	          res.send(html);
});

// app.post('/lottoupload' , upload.single('xlxs_file'), function(req,res){
// 	console.log(req.file);
// 	res.send('uploadfile : '+req.file);
// });

app.post('/lottoupload', upload.array('xlxs_file',1),function(req,res){
	console.log('req:', req.body); 
	console.log('query: ', req.query);
	console.log('params: ', req.params);
	const files = req.files;
	let originalName = '';
	let fileName = '';
	let mimeType = '';
	let size = '';
	let worksheet = '';

	if(Array.isArray(files)){
		console.log('files is array!');
		console.log(files[0].path);

		originalName = files[0].originalname;
		fileName = files[0].filename;
		mimeType = files[0].mimetype;
		size = files[0].size;
		var workbook = XLSX.readFile(files[0].path);
		var sheet_name = workbook.SheetNames[0];
		worksheet = workbook.Sheets['excel'];
		console.log('workbook : '+workbook);
		console.log('sheet_name : '+sheet_name);
		

	} else {
		console.log('files is not array!');
	}
	
	res.send("?");
	// res.redirect('/readlotto');
	// res.writeHead(200, {'Content-Type':'text/plain; charset=utf-8'});
	// res.end('worksheet 의 값은: ' + worksheet);
	// res.json(worksheet);
	console.log(`file inform : ${originalName}, ${fileName}, ${mimeType}, ${size}`);
	console.log(worksheet);
});



var port = process.env.PORT ||3000;
app.listen(port, () => {
  console.log('start');
});
